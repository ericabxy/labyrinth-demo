# Copyright 2015, 2016, 2017 Eric Duhamel

# This file is part of Labyrinth.

# Labyrinth is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Labyrinth is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Labyrinth.  If not, see <http://www.gnu.org/licenses/>.

import os

import labyrinthGame

DISPLAYWIDTH = 640
DISPLAYHEIGHT = 480
FPS = 30
BGCOLOR = (33, 30, 39)
#BGCOLOR = (0, 0, 0)

HUBMODE = 'hubmode'
COOPMODE = 'coopmode'

def drawTileMap(gamestate, gfx):
    gfx.drawTileMap('walls', gamestate.getFieldRect(),
                    gamestate.getTileMap(), gfx.BACKGROUND)
    gfx.drawTileMap('foreg', gamestate.getFieldRect(),
                    gamestate.getTileMap(), gfx.FOREGROUND)

# Coordinates for the off-battlefield position of each player
#SCORESX = (96, 360)
SCORESX = (466, 16)
SCORESXPLUS = 86
LIVESXPLUS = 16
SCOREY = 16
LIVESY = 18
def showPlayerScores(scores, gfx):
    # Unique colors associated with each player number
    PLAYERCOLORS = (
        gfx.AQUA,
        gfx.FUCHSIA,
        gfx.MAROON,
        gfx.SILVER
    )
    for p, player in enumerate(scores):
        # Get display stats for player
        playerColor = PLAYERCOLORS[p]
        scoreX = SCORESX[p]
        scoreWidth = 72
        scoreHeight = 24
        # Blank the lives first
        gfx.showTextBox(
            "Lives: " + str(player['lives']),
            gfx.BLACK, BGCOLOR,
            scoreX, SCOREY, scoreWidth, scoreHeight,
            gfx.NORMALFONTSIZE+6, os.path.join('data', 'romulus.ttf')
        )
        # Draw lives
        for lifeNum, spriteName in enumerate(player['list']):
            gfx.showImage(
                os.path.join('data', spriteName + '.png'),
                scoreX + (lifeNum * LIVESXPLUS), LIVESY
            )
        # Only need to draw score once
        gfx.showTextBox(
            str(player['score']),
            playerColor, BGCOLOR,
            scoreX + SCORESXPLUS, SCOREY, scoreWidth, scoreHeight,
            gfx.NORMALFONTSIZE+6, os.path.join('data', 'romulus.ttf')
        )

def addAllAssets(sound, gfx, SCALE):
    allSpriteSets = getAllSpriteSets()
    allTileSets = getAllTileSets()
    for spriteSet in allSpriteSets:
        gfx.addSpriteSheet(
            os.path.join('data', spriteSet['filename']),
            spriteSet['name'],
            spriteSet['size'],
            spriteSet['size'],
            spriteSet['steps'],
            SCALE
        )
    for tileSet in allTileSets:
        xparams = {'scale': SCALE,}
        if 'overlapheight' in tileSet:
            xparams['overlapheight'] = tileSet['overlapheight']
        gfx.addTileSet(
            os.path.join('data', tileSet['filename']),
            tileSet['name'],
            tileSet['width'],
            tileSet['height'],
            xparams
        )
    sound.addSoundEffect('shot01', os.path.join('data', 'Shot_00.ogg'))
    sound.addSoundEffect('shot02', os.path.join('data', 'Stab_Knife_00.ogg'))
    sound.addSoundEffect('burst01', os.path.join('data', 'Stab_Knife_02.ogg'))
    sound.addSoundEffect('open01', os.path.join('data', 'Inventory_Open_01.ogg'))
    sound.addSoundEffect('death01', os.path.join('data', 'Explosion_01.ogg'))
    sound.addSoundEffect('death02', os.path.join('data', 'Explosion_00.ogg'))
    sound.addSoundEffect('levelstart01', os.path.join('data', 'Jingle_Achievement_00.ogg'))
    sound.addSoundEffect('gameover01', os.path.join('data', 'Jingle_Lose_00.ogg'))
    sound.addSoundList(
        'dirt09',
        os.path.join('data', 'Footstep_Dirt_0'), 10, '.ogg'
    )
    sound.addPlaylist('data', 'Track_0', 2, '.ogg')

def playSoundEffects(sound, spriteobjs, gamemode, gamestatus, gamestep):
    PLAYER = 'player'
    MISSILE = 'missile'
    CASTING = 'casting'
    MOVING = 'moving'
    ARRIVING = 'arriving'  
    DYING = 'dying'
    HUBMODE = 'hubmode'
    STARTING = 'starting'
    GAMEOVER = 'gameover'
    for spriteObj in spriteobjs:
        type = spriteObj['type']
        name = spriteObj['name']
        status = spriteObj['status']
        step = spriteObj['step']
        if type == PLAYER:
            if status == CASTING and step == 3:
                sound.playSoundEffect('shot01')
            elif status == MOVING and step in (1, 7):
                sound.playRandomSound('dirt09')
#            elif status == ARRIVING:
#                if step == 0:
#                    sound.playSoundEffect('open01')
            elif status == DYING and step == 0:
                    sound.playSoundEffect('death02')
        if type == MISSILE:
            if status == MOVING:
                if name == "dark-missile-moving":
                    if step == 0:
                        sound.playSoundEffect('shot02')
            if status == DYING:
                if name == "dark-missile-dying":
                    if step == 1:
                        sound.playSoundEffect('burst01')
                else:
                    if step == 10:
                        sound.playSoundEffect('death01')
    if gamestep == 4 and gamemode != HUBMODE:
        if gamestatus == STARTING:
            sound.playSoundEffect('levelstart01')
        elif gamestatus == GAMEOVER:
            sound.playSoundEffect('gameover01')

def getAllSpriteSets():
    return labyrinthGame.getAllObjStates()

def getAllTileSets():
    tileSet1 = {
        'filename': 'wallsBG.png',
        'name': 'walls',
        'width': 32,
        'height': 48,
        'overlapheight': 12
    }
    tileSet2 = {
        'filename': 'wallsFG.png',
        'name': 'foreg',
        'width': 32,
        'height': 32
    }
#    allTileSets = (tileSet1, tileSet2)
    allTileSets = (tileSet1,)
#    gfx.addTileSet('data/wallTiles-32x32.png', 'walls', 32, 48,
#                   {'scale': SCALE, 'overlapheight': 12})
#    gfx.addTileSet('data/wallTilesf-32x32.png', 'foreg', 32, 32,
#                   {'scale': SCALE,})
    return allTileSets

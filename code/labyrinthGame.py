# Copyright 2015, 2016, 2017 Eric Duhamel

# This file is part of Labyrinth.

# Labyrinth is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Labyrinth is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Labyrinth.  If not, see <http://www.gnu.org/licenses/>.

import pygame, time, copy

import labyrinthObjects

# Controller constants
STICK = 'stick'
BUTTON = 'button'

# Game states
STARTING = 'starting'
PLAYING = 'playing'
ENDING = 'ending'
GAMEOVER = 'gameover'
STARTCYCLE = 0
STARTINGCYCLE = 60
ENDINGCYCLE = 60
DYINGTIME = 2

MAXMONSTERS = 4
MONSTERSPAWNTIME = 24

# Event codes to return during the game
# TODO: all of these should be part of GamePlayObject()
NOEVENT = 0
STARTEVENT = 1
SCOREEVENT = 2
DEATHEVENT = 3
ENDEVENT = 4
GAMEOVEREVENT = 5
LEAVEGAMERECT = pygame.Rect(256, 64, 128, 64)  # Get rid of this magic Rect
STARTGAMERECT = pygame.Rect(256, 336, 128, 64)  # Get rid of this magic Rect
START1PCOOP = 6
START2PCOOP = 7
ENDCOOPGAME = 8

COOPMODE = 'coopmode'
HUBMODE = 'hubmode'

class GamePlayObject(labyrinthObjects.LabyrinthGame):
    def __init__(self, windowwidth, windowheight, numplayers, mode):
        width = 320
        height = 240
#        super(GamePlayObject, self).__init__(windowwidth, windowheight)
        super(GamePlayObject, self).__init__(width, height)
        self.setScale(windowwidth, windowheight)
        self.setSizes(self.scale)
        self.setOutpositions()
#        print(
#            "SPRITESIZE: " + str(self.SPRITESIZE)
#            + ", TILESIZE: " + str(self.TILESIZE)
#        )
        self.WINDOWWIDTH = windowwidth
        self.WINDOWHEIGHT = windowheight
        self.numPlayers = numplayers
        self.mode = mode
        makeQueue = []
        # Make a wave of monsters equal to MAXMONSTERS
        for i in range(MAXMONSTERS):
            makeQueue.append(self.GHOUL)
        self.initialQueue = tuple(makeQueue)
        self.monsterQueue = []
        self.monsterObjs = []
        self.monsterSpawnTimer = 0
        # Create the first maze
        if self.mode == COOPMODE:
            self.maze = labyrinthObjects.Maze4(
                self.TILESIZE, self.SPRITESIZE, windowwidth, windowheight,
                {'yoffset': 8,}
            )
        elif self.mode == HUBMODE:
            self.maze = labyrinthObjects.HubMaze1(
                self.TILESIZE, self.SPRITESIZE, windowwidth, windowheight,
                {'yoffset': 16,}
            )
        self.endLevelTime = time.time()
        # Add all the Players designated for this game
        self.playerObjs = []
        self.spriteList = pygame.sprite.Group()
        for p in range(self.numPlayers):
            i = p
            playerNames = 2
            while i > playerNames:
                i -= 2
            newPlayer = self.getNewPlayer(p, 0)
            self.playerObjs.append(newPlayer)
            newSprite = pygame.sprite.Sprite()
            newSprite.rect = newPlayer
            self.spriteList.add(newSprite)
        # Start the game
        self.wave = 0
        self.status = STARTING
        self.step = STARTCYCLE-1
        self.SPAWNEVENT = 9
        self.MONSTERSPEEDS = {
            'normal': self.BASEMONSTERSPEED,
            'fast': self.BASEMONSTERSPEED+1,
            'faster': self.BASEMONSTERSPEED+2,
            'fastest': self.BASEMONSTERSPEED+3
        }
        self.monsterSpeed = self.MONSTERSPEEDS['normal']
        self.speedMultiplier = 1
        self.speedBooster = 0
        self.speedUpCounter = 0

    def playGame(self, controlmap):
        """Play a game of Labyrinth of the Dead.

        This method performs ONE STEP of the main game, then returns.
        One step of the game is organized into phases, each representing a
        main actor of the game: players, monsters, and game status.

        Each phase is divided into an order of operations which should be
        as follows:

        1: Create any characters or objects.
        2: Resolve player input or artificial intelligence.
        3: Move any moveable objects.
        4: Resolve collisions between objects.
        5: Remove any un-needed objects."""
        returnValue = NOEVENT
        if self.status == STARTING:
            if self.step < STARTINGCYCLE:
                self.step += 1
            else:
                self.status = PLAYING
                returnValue = STARTEVENT
                self.step = -1
            # Add all the MONSTERS to the queue for this wave
            if len(self.monsterObjs) < 1:
                if len(self.monsterQueue) < 1:
                    self.monsterQueue = list(self.initialQueue)
                    for i in range(self.wave+1):
                        self.monsterQueue.append(self.GHOUL)
            self.monsterSpeed = self.BASEMONSTERSPEED
        elif self.status == ENDING:
            if self.step < ENDINGCYCLE:
                self.step += 1
            else:
                self.status = STARTING
                returnValue = STARTEVENT
                self.step = -1
            if self.step == ENDINGCYCLE:
                self.wave += 1
        elif self.status == GAMEOVER:
            if self.step < 256:
                self.step += 1
        elif self.status == PLAYING and self.mode == COOPMODE:
            if len(self.monsterObjs) < 1:
                self.status = ENDING
                returnValue = ENDEVENT
                self.step = STARTCYCLE
            elif self.step < 1024:
                self.step += 1
        gameStatus = self.status

        # MONSTERS PHASE
        if gameStatus in (STARTING, PLAYING, GAMEOVER):
            for monster in self.monsterObjs:
                if monster.status not in monster.OBJECTINERT:
                    # Decide on a direction and move
                    intentDirection = monster.aiChooseDirection(self.maze, self.playerObjs)
                    cell = self.maze.getCell(monster.x, monster.y)
#                    monster.moveOnPath(self.maze, intentDirection, self.monsterSpeed)
                    monster.moveOnPath(
                        self.maze, intentDirection, self.monsterSpeed
                    )
                    if not cell.collidepoint(monster.x, monster.y):
                        monster.aiUndecide()
                        #cellX, cellY = self.maze.getBoxAtPixel(monster.x, monster.y)
                        #print("New cell: " + str(cellX) + "x" + str(cellY) + ": " + str(cell.getWalls()))
                        #paths = self.maze.getAllPaths(monster.x, monster.y)
                        #print(self.maze.getVerbalPaths(paths))
                    # Decide when to fire missile at players
                    monster.moveMissile(self.maze)
                    if monster.aiDecideFire(self.maze, self.playerObjs):
#                        print("Decide Fire")
                        missilesFlying = 0
                        for m in self.monsterObjs:
                            if m.missile.fired:
                                missilesFlying += 1
                        # Make sure the total number of missiles and monsters
                        # doesn't exceed maximum
                        if (missilesFlying < 2
                                and (len(self.monsterObjs) + missilesFlying)
                                < MAXMONSTERS):
                            monster.fireMissile()
                # Step through the action cycles
                monster.advanceCycles()
                # Resolve any collisions with Player missiles
                for player in self.playerObjs:
                    if player.missile.fired:
                        if player.missile.status not in player.OBJECTINERT:
                            if monster.status not in monster.OBJECTINVULNERABLE:
                                if player.missile.getCollideRect().colliderect(monster.getCollideRect()):
                                    player.score += monster.value
                                    returnValue = SCOREEVENT
                                    monster.startCycle(monster.DYING)
                                    player.explodeMissile(monster.center)
                if monster.status == monster.DEAD:
                    # Remove monster if dead
                    self.monsterObjs.remove(monster)
                    # Speed up remaining monsters
                    self.speedUpCounter += 1
                    if self.speedUpCounter > 2:
                        self.monsterSpeed = self.MONSTERSPEEDS['faster']
                        self.speedUpCounter = 0
                    # Max speed last monster for challenge
                    if len(self.monsterObjs) < 2:
                        self.monsterSpeed = self.MONSTERSPEEDS['fastest']
            # Add monsters to the maze
            if self.mode == COOPMODE:
                if len(self.monsterObjs) < MAXMONSTERS:
                    if self.monsterSpawnTimer > MONSTERSPAWNTIME:
                        # Move monsters from the queue into play
                        if len(self.monsterQueue) > 0:
                            if self.spawnOneMonsterFrom(self.monsterQueue):
                                returnValue = self.SPAWNEVENT
                        # Set the level to end if we're out of monsters
                        elif len(self.monsterObjs) < 1:
                            self.monsterSpeed = self.MONSTERSPEEDS['normal']
                            self.speedUpCounter = 0
                            self.endLevelTime = time.time()
                self.monsterSpawnTimer += 1

        # PLAYERS PHASE
        if (self.status == GAMEOVER and self.step >= 128
            and controlmap[0][BUTTON]):
                returnValue = ENDCOOPGAME
        if gameStatus in (STARTING, PLAYING, ENDING):
            for playerNum, player in enumerate(self.playerObjs):
                # Resolve player controls
                if player.status in player.PLAYERMOVEABLE:
                    stickDelta = controlmap[playerNum][STICK]
                    if stickDelta in self.DELTA2DIRECT:
                        stickDirection = self.DELTA2DIRECT[stickDelta]
                    fireButton = controlmap[playerNum][BUTTON]
                    if (stickDirection in self.ANYORTHOGONAL or
                            stickDirection in self.ANYDIAGONAL):
                        intentDirection = self.getOrthogonal(stickDirection, player.direction)
                        cell = self.maze.getCell(player.x, player.y)
                        player.moveOnPath(self.maze, intentDirection,
                                          self.PLAYERSPEED)
                        if not cell.collidepoint(player.x, player.y):
                            #cellX, cellY = self.maze.getBoxAtPixel(player.x, player.y)
                            #print("New cell: " + str(cellX) + "x" + str(cellY) + ": " + str(cell.getWalls()))
                            paths = self.maze.getAllPaths(player.x, player.y)
                            #print(self.maze.getVerbalPaths(paths))
                        # Start the MOVING walkcycle if not started
                        if player.status != player.MOVING:
                            player.startCycle(player.MOVING)
                    # Switch to STANDING if the player is keeping still
                    elif player.status != player.INVULNERABLE:
                        # Make player face camera if ready to start
#                        if self.getPlayersReady() > playerNum:
#                            if self.mode == HUBMODE:
#                                player.direction = player.DOWN
                        player.startCycle(player.STANDING)
                    # What to do if fire button was pressed
                    if fireButton:
                        # Start the game
                        if self.mode == HUBMODE:
                            # Start the number of players depending on
                            # how many are in the doorways
                            if playerNum == 0:
                                if self.getPlayersReady() == 1: 
                                    returnValue = START1PCOOP
                                elif self.getPlayersReady() == 2: 
                                    returnValue = START2PCOOP
                        # Start firing missile
                        if not player.missile.fired:
                            player.startCycle(player.CASTING)
                # Step through the action cycles
                player.advanceCycles()
                player.moveMissile(self.maze)
                # Check for player collisions with killing objects
                for monster in self.monsterObjs:
                    if monster.status not in monster.OBJECTINERT:
                        if player.status not in player.OBJECTINVULNERABLE:
                            # Collision with monster
                            if monster.getCollideRect().colliderect(player.getCollideRect()):
                                player.startCycle(player.DYING)
                                player.dyingStartTime = time.time()
                            # Collision with monster missile
                            elif (monster.missile.fired and 
                                monster.missile.status not in 
                                monster.OBJECTINERT):
                                    if monster.missile.getCollideRect().colliderect(player.getCollideRect()):
                                        player.startCycle(player.DYING)
                                        player.dyingStartTime = time.time()
                                        monster.explodeMissile(player.center)
                # If player is dead, respawn or endgame
                if (player.status == player.DYING and
                        time.time() - player.dyingStartTime > DYINGTIME or
                        player.status == player.DEAD):
                    if player.respawns > 0:
                        # While player is still dead, find a random spawn point
                        if self.mode == COOPMODE:
                            player.center = self.maze.getPlayerSpawnPoint()
                        elif self.mode == HUBMODE:
                            player.center = self.maze.getPlayerSpawnPoint(
                                player.playerNum
                            )
                        # Spawn if there are no others nearby
                        if not self.getNearby(player):
                            #player.name = player.respawnList[player.respawns-1].name
                            #del player.respawnList[player.respawns-1]
                            #player.respawns -= 1
                            #if player.status != player.DEAD:
                            #    player.startCycle(labyrinthObjects.ARRIVING)
                            #else:
                            #    player.startCycle(labyrinthObjects.STANDING)
                            #player.y += 2
                            player.doRespawn()
                            returnValue = DEATHEVENT
                    else:
                        gameIsReallyOver = True
                        for p in self.playerObjs:
                            if p.respawns > -1:
                                gameIsReallyOver = False
                        if gameIsReallyOver:
                            self.status = GAMEOVER
                            returnValue = GAMEOVEREVENT
                            self.step = -1
                            self.monsterSpeed = self.MONSTERSPEEDS['normal']

        # Done with this game cycle
        # Return True and wait to start the next cycle
        return returnValue

    def getFieldRect(self):
        return self.maze

    def getTileMap(self):
        return self.maze.wallmap

    def getPlayerScores(self):
        playerScores = []
        for player in self.playerObjs:
            respawnList = []
            for respawn in player.respawnList:
                respawnList.append(respawn.name)
            playerScores.append({'score': player.score, 'lives': player.respawns, 'list': tuple(respawnList)})
        return tuple(playerScores)

    def getPlayersReady(self):
        if (len(self.playerObjs) > 1
                and STARTGAMERECT.contains(self.playerObjs[1])
                and STARTGAMERECT.contains(self.playerObjs[0])):
            return 2
        elif (len(self.playerObjs) > 0
                and STARTGAMERECT.contains(self.playerObjs[0])):
            return 1
        else:
            return 0

    def getGameStatus(self):
        return {
            'status': self.status,
            'step': self.step,
            'level': self.wave+1,
            'maxlevel': 8,  # TODO: magic number
            'monsterqueue': self.monsterQueue,
            'monsterspeed': self.monsterSpeed
        }

    def getAllObjects(self):
        allObjs = []
        for playerObj in self.playerObjs:
            allObjs.append(playerObj)
            if playerObj.missile.fired:
                allObjs.append(playerObj.missile)
        for monsterObj in self.monsterObjs:
            allObjs.append(monsterObj)
            if monsterObj.missile.fired:
                allObjs.append(monsterObj.missile)
        return allObjs

    def getAllObjs(self):
        allObjs = []
        for playerObj in self.playerObjs:
            allObjs.append(playerObj)
            if playerObj.missile.fired:
                allObjs.append(playerObj.missile)
        for monsterObj in self.monsterObjs:
            allObjs.append(monsterObj)
            if monsterObj.missile.fired:
                allObjs.append(monsterObj.missile)
#        return super(GamePlayObject, self).getAllObjs("-")
        allObjDicts = []
        for obj in allObjs:
            objDict = {}
            objDict['name'] = obj.name + "-" + obj.status
            objDict['type'] = obj.type
            objDict['x'] = obj.x
            objDict['y'] = obj.y
            objDict['status'] = obj.status
            objDict['step'] = obj.step
            if obj.status in (obj.ARRIVING,
                              obj.INVULNERABLE,
                              obj.DYING):
                objDict['direction'] = 0
            else:
                objDict['direction'] = obj.direction
            if obj.status != 'dead':
                allObjDicts.append(objDict)
        return allObjDicts

    def getNearby(self, subject):
        objects = []
        for m in self.monsterObjs:
            if m != subject:
                objects.append(m)
        for p in self.playerObjs:
            if p != subject:
                objects.append(p)
        for object in objects:
            # TODO: get rid of this magic number
            if abs(subject.x - object.x) + abs(subject.y - object.y) < 48:
                return True
        return False

    def spawnOneMonsterFrom(self, list):
        x, y = self.maze.getMonsterSpawnPoint()
        walls = self.maze.getCell(x, y).getWalls()
        futureMonster = self.getMonster(
            self.GHOUL, x, y
        )
        # TODO: get rid of this magic number 18.
        # Spawn if there are no others nearby
        if not self.getNearby(futureMonster) and walls != 18:
            # Remove monster name from list and creat a new monster from it
            newMonster = self.getMonster(
                list.pop(0), x, y
            )
            newMonster.centerSelf()
            self.monsterObjs.append(newMonster)
            newSprite = pygame.sprite.Sprite()
            newSprite.rect = newMonster
            self.spriteList.add(newSprite)
            self.monsterSpawnTimer = 0
            return True
        return False


# This function returns the names of all the actions and the number of
# game cycles in each, for Main to load graphics.
# SIZES are here temporarily, to be deleted later
SIZE1 = 18
def getAllObjStates():
    statesList = []
    dummyObj = labyrinthObjects.LabyrinthObject('dummy', (0, 0, 18, 18))
    for obj in dummyObj.MONSTERS:
        for state in dummyObj.MONSTERCYCLES:
            monsterState = {
                'filename': obj + '-' + state + '.png',
                'name': obj + '-' + state,
                'size': SIZE1,
                'steps': dummyObj.MONSTERCYCLES[state]
            }
            statesList.append(monsterState)
    for obj in dummyObj.PLAYERS:
        for state in dummyObj.PLAYERCYCLES:
            playerState = {
                'filename': obj + '-' + state + '.png',
                'name': obj + '-' + state,
                'size': SIZE1,
                'steps': dummyObj.PLAYERCYCLES[state]
            }
            statesList.append(playerState)
    for obj in dummyObj.MISSILES:
        for state in dummyObj.missile.CYCLES:
            missileState = {
                'filename': obj + '-' + state + '.png',
                'name': obj + '-' + state,
                'size': SIZE1,
                'steps': dummyObj.missile.CYCLES[state]
            }
            statesList.append(missileState)
    return statesList

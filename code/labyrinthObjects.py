# Copyright 2015, 2016, 2017 Eric Duhamel

# This file is part of Labyrinth.

# Labyrinth is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Labyrinth is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Labyrinth.  If not, see <http://www.gnu.org/licenses/>.

import pygame, random, time
random.seed()
# TODO: importing random is now deprecated since game objects have
# randomizing methods

from ebigo import gameobjects

# Sprite status
#STARTING = 'starting'
#ARRIVING = 'arriving'
#STANDING = 'standing'
#MOVING = 'moving'
#FLYING = MOVING
#CASTING = 'casting'
#DYING = 'dying'
#DEAD = 'dead'
#ORBITING = 'orbiting'
#DEPARTING = 'departing'
#RESPAWNING = 'respawning'
#NOTHING = 'nothing'

# Timers and action cycles
CYCLE1 = 12
CYCLE2 = 24
CYCLE3 = 36
CYCLE4 = 48
STANDARDCYCLE = CYCLE1
INVULNERABLETIME = 9

# Wall tile index numbers
NORTH = 1 # 0001
EAST = 2 # 0010
SOUTH = 4 # 0100
WEST = 8 # 1000
OO = 39
NN = 3
NNN = 26
NE = 7
NNEE = 37
EE = 28
EEE = 11
SE = 33
SSEE = 18
SS = 46
SSS = 23
NS = 40
NNS = 25
SNS = 5
SW = 43
SSWW = 13
WW = 15
WWW = 38
NW = 32
NNWW = 20
WE = 34
EWE = 29
WWE = 31
# Doors
DNW = 65
DNE = 98
# Spawn points
SP1 = 20 # Spawn Point 1
SP2 = 25
SP3 = 31
SP4 = 41
# Odd walls (deprecated)
WSW = 45
SSW = 22
NNW = 36
WNW = 6
ESE = 44
SSE = 14
NNE = 21
ENE = 4

# DEPRECATED: global: directions corresponding to each compass direction
#COMPASS = {UP: NORTH, LEFT: WEST, DOWN: SOUTH, RIGHT: EAST}

# Sprite names
PLAYER = 'player'
HEALERM = 'healer-m'
HEALERF = 'healer-f'
MAGEM = 'mage-m'
MAGEF = 'mage-f'
MISSILE = 'missile'
HOLYMISSILE = 'holy-missile'
MAGICMISSILE = 'magic-missile'
DARKMISSILE = 'dark-missile'
MONSTER = 'monster'
# Lesser Monsters
#GHOUL = 'ghoul'
GHAST = 'ghast'
WIGHT = 'wight'
# Bosses
GHOST = 'ghost'
SHADE = 'shade'
#MONSTERS = (GHOUL, GHAST, WIGHT)
#MISSILES = (HOLYMISSILE, MAGICMISSILE, DARKMISSILE)

# Speed constants
SPEED1 = 1
SPEED2 = 2
SPEED3 = 3
SPEED4 = 5
SPEED5 = 6

# DEPRECATED
#GODIRECTIONS = {LEFT: gameobjects.LEFT, DOWN: gameobjects.DOWN, UP: gameobjects.UP, RIGHT: gameobjects.RIGHT}
# DEPRECATED
#OPPOSITE = {UP: DOWN, LEFT: RIGHT, DOWN: UP, RIGHT: LEFT}
#VERBAL = {UP: 'up', LEFT: 'left', DOWN: 'down', RIGHT: 'right'}

class LabyrinthGame(gameobjects.PlayObject):
    """Superclass for labyrinthGame.Game()."""
    def __init__(self, width, height):
        super(LabyrinthGame, self).__init__(width, height)
#        self.setSizes(self.scale)
#        self.setOutpositions()
        self.STARTCYCLE = 0
        self.RESTARTCYCLE = 1
        # Lesser Monsters
        # TODO: should LabyrinthGame need this, or just LabyrinthObject?
        self.GHOUL = 'ghoul'
        self.GHAST = 'ghast'
        self.WIGHT = 'wight'
        return None

    def setSizes(self, scale):
        self.TILESIZE = (self.width * scale) / 10
        # Original Monster speed was 1px on a 48 px tile
        # 1px seems to match with the animation
        self.BASEMONSTERSPEED = int(1.33*scale)
        # Original Player speed was 3px on a 48px tile
        self.PLAYERSPEED = 2*scale
        # Original Missile speed was 8px on a 48px tile
        self.MISSILESPEED = 4*scale
        self.SPRITESIZE = 18*scale

    def setOutpositions(self):
        # Coordinates for the off-battlefield position of each player
        OUTPOSITIONS1 = []
        OUTPOSITIONS2 = []
        for i in range(64, 144, 16):
            OUTPOSITIONS1.append((i, 24, self.SPRITESIZE, self.SPRITESIZE))
        for i in range(328, 408, 16):
            OUTPOSITIONS2.append((i, 24, self.SPRITESIZE, self.SPRITESIZE))
        self.OUTPOSITIONS = (OUTPOSITIONS1,
                             OUTPOSITIONS2)

    def getNewPlayer(self, playernum, respawn):
#        respawnRect = self.OUTPOSITIONS[playernum][respawn]
        (x, y, width, height) = self.OUTPOSITIONS[playernum][respawn]
        return NewPlayer(playernum, x, y, width, height)

    def getMonster(self, type, x, y):
        width = self.SPRITESIZE
        height = self.SPRITESIZE
        if type == self.GHOUL:
            monster = Ghoul(x, y, width, height)
        elif type == GHAST:
            monster = Ghast(x, y, width, height)
        elif type == WIGHT:
            monster = Wight(x, y, width, height)
        else:
            # Default monster is Ghoul
            print("Monster " + type + " not found")
            monster = Ghoul(x, y, width, height)
        return monster


class LabyrinthObject(gameobjects.Shooter):
    """Superclass for Labyrinth Players and Monsters."""
    def __init__(self, name, rect):
        super(LabyrinthObject, self).__init__(name, rect)
#        print("LabyrinthObject: "
#            + str(self.x) + ", "
#            + str(self.y) + ", "
#            + str(self.width) + ", "
#            + str(self.height)
#        )
        # Sprite names
        self.PLAYER = 'player'
        self.HEALERM = 'healer-m'
        self.HEALERF = 'healer-f'
        self.MAGEM = 'mage-m'
        self.MAGEF = 'mage-f'
        self.MONSTER = 'monster'
        # Lesser Monsters
        self.GHOUL = 'ghoul'
        self.GHAST = 'ghast'
        self.WIGHT = 'wight'
        # Bosses
        self.GHOST = 'ghost'
        self.SHADE = 'shade'
        self.MONSTERS = (self.GHOUL, self.GHAST, self.WIGHT)
        self.PLAYERS = (self.HEALERM, self.HEALERF, self.MAGEM, self.MAGEF)
        self.HEALERS = (self.HEALERM, self.HEALERF)
        self.MAGES = (self.MAGEM, self.MAGEF)
        self.PLAYERS1 = self.HEALERS
        self.PLAYERS2 = self.MAGES
        self.PLAYERS3 = ()
        self.PLAYERS4 = ()
        self.PLAYERNAMES = (self.PLAYERS1, self.PLAYERS2)
        self.MISSILE = 'missile'
        self.HOLYMISSILE = 'holy-missile'
        self.MAGICMISSILE = 'magic-missile'
        self.DARKMISSILE = 'dark-missile'
        self.MISSILES = (self.HOLYMISSILE, self.MAGICMISSILE, self.DARKMISSILE)
        self.setScale(self.width, self.height)
        # Shrink factor for collisions
        self.shrinkW = -20
        self.shrinkH = -20
        # Names for each object status
        self.DYING= "dying"
        self.DEAD = "dead"
        self.ARRIVING = 'arriving'
        self.INVULNERABLE = 'invulnerable'
        self.STANDING = 'standing'
        self.MOVING = 'moving'
        self.CASTING = 'casting'
        self.DEPARTING = 'departing'
        self.NOTHING = 'nothing'
        # Each object has a missile
        self.missile.type = MISSILE
        self.missile.name = MISSILE
        self.missile.direction = self.LEFT
        self.missile.width = self.width
        self.missile.height = self.height
        self.missile.shrinkW = -24
        self.missile.shrinkH = -24
        self.missile.ARRIVING = self.ARRIVING
        self.missile.INVULNERABLE = self.INVULNERABLE
        self.missile.DYING = self.DYING
        self.missile.status = self.NOTHING
        # How many steps are taken for each action
        # TODO: convert this to object constants
        self.PLAYERCYCLES = {
            self.ARRIVING: 24, self.INVULNERABLE: 12, self.STANDING: 12,
            self.MOVING: 12, self.CASTING: 12, self.DYING: 24,
            self.DEPARTING: 48
        }
        self.MONSTERCYCLES = {
            self.ARRIVING: 36, self.MOVING: 12, self.DYING: 12
        }
        # Some constants to group object states
        self.OBJECTINVULNERABLE = (
            self.ARRIVING, self.INVULNERABLE, self.DYING, self.DEPARTING
        )
        self.OBJECTINERT = (
            self.ARRIVING, self.INVULNERABLE, self.DYING, self.DEPARTING,
            self.DEAD)
        self.PLAYERIMMOVEABLE = (
            self.ARRIVING, self.CASTING, self.DEPARTING, self.DYING, self.DEAD
        )
        self.PLAYERMOVEABLE = (
            self.INVULNERABLE, self.STANDING, self.MOVING)
        # TODO: each subclass should determine its own self.CYCLES
        # based on superclass self.*CYCLES
        self.CYCLES = {
            self.ARRIVING: 24,
        }
        self.missile.CYCLES = {
            self.MOVING: 12, self.DYING: 16, self.DEPARTING: 12
        }
        self.STARTCYCLE = 0
        self.RESTARTCYCLE = 1

    def setScale(self, width, height):
        baseSize = 18
        size = width
        if size < baseSize*2:
            scale = 1
        elif size < baseSize*3:
            scale = 2
        elif size < baseSize*4:
            scale = 3
        else:
            scale = 1
        self.MISSILESPEED = 4*scale

    def getCollideRect(self):
        """Return a shrunken rect respresenting a smaller collision box."""
        return self.inflate(self.shrinkW, self.shrinkH)

    def moveOnPath(self, wallmaze, intentdirection, speed):
        """Move along center of maze path, stopping if there is a wall."""
#        cellRect = wallmaze.getCellRect(self.centerx, self.centery)
        cell = wallmaze.getCell(self.centerx, self.centery)
        cellRect = cell.getRect()
        cellWalls = cell.getWalls()
        # Temporarily offset the position of the character
#        offset = 6
        offset = -2
        self.y -= offset
        # Get direction and distance to center of cell
        newDirect, newDist = self.getCenterPath(cellRect, intentdirection, speed)
        # Make sure this doesn't reverse player direction
        if newDirect == self.DIRECT2OPPOSITE[self.direction]:
            if intentdirection != newDirect:
                newDirect = self.direction
        # Get distance to any blocking wall
        COMPASS = {
            self.UP: NORTH,
            self.LEFT: WEST,
            self.DOWN: SOUTH,
            self.RIGHT: EAST
        }
        if cellWalls in wallmaze.WALLSDICT[COMPASS[newDirect]]:
            newDist = self.getBlockedPath(cellRect, newDirect, newDist)
        # Change player direction
        if newDist > 0:
            self.direction = newDirect
        # Move player
        self.moveDirection(newDirect, newDist)
        self.y += offset
        # Return the direction and distance in case it's needed
        return newDirect, newDist

    def fireMissile(self):
        """Activate the missile and launch from center of shooter."""
        super(LabyrinthObject, self).fireMissile(self.centerx, self.centery)
        self.missile.direction = self.direction
        self.missile.status = self.MOVING
        self.missile.step = self.STARTCYCLE

    def explodeMissile(self, center):
        """Destroy the missile (usually while killing a target)."""
        self.missile.width = 32
        self.missile.height = 32
        self.missile.center = center
        self.missile.status = self.DYING
        self.missile.step = self.STARTCYCLE

    def departMissile(self):
        """Remove the missile from play (usually without killing anything)."""
        if self.missile.status not in (self.DEPARTING, self.DYING):
            self.missile.moveDirection(
                self.DIRECT2OPPOSITE[self.missile.direction], 12
            )
            self.missile.status = self.DEPARTING
            self.missile.step = self.STARTCYCLE

    def moveMissile(self, wallmaze):
        """Move the missile if it has been fired.

        Also resolve collisions with playing field boundaries and walls.
        """
        # TODO: get rid of these global variables
        if self.missile.fired:
            missileDirection = self.missile.direction
            if self.missile.status == self.DYING:
                if self.missile.step < self.missile.CYCLES[self.DYING]:
                    self.missile.step += 1
                else:
                    self.missile.step = 0
                    self.stopMissile()
            elif self.missile.status == self.DEPARTING:
                if self.missile.step < self.missile.CYCLES[self.DEPARTING]:
                    self.missile.step += 1
                else:
                    self.missile.step = 0
                    self.stopMissile()
            else:
                super(LabyrinthObject, self).moveMissile(
                    missileDirection, self.MISSILESPEED
                )
                if self.missile.step < self.missile.CYCLES[self.MOVING]:
                    self.missile.step += 1
                else:
                    self.missile.step = self.RESTARTCYCLE
            # Check for missile collision with boundaries
            if (not wallmaze.contains(self.missile.getCollideRect())
                or not wallmaze.getPathIsOpen(self.missile.direction,
                self.missile)):
                    self.departMissile()

    def advanceCycles():
        """Step through the currect action cycle."""
        if self.status == self.ARRIVING:
            if self.step < self.CYCLES[self.ARRIVING]:
                self.step += 1
            else:
                self.status = self.INVULNERABLE
                self.step = self.STARTCYCLE
                self.direction = self.DOWN
                self.respawnStartTime = time.time()
        elif self.status == MOVING:
            if self.step < PLAYERCYCLES[MOVING]:
                self.step += 1
            else:
                self.step = self.RESTARTCYCLE
        elif self.status == DYING:
            if self.step < PLAYERCYCLES[DYING]:
                self.step += 1


class Player(LabyrinthObject):
    def __init__(self, number, rect):
        self.respawnStartTime = time.time()
        self.dyingStartTime = 0
        self.score = 0
        self.step = 0
        # HACK: Player somehow needs to know its name before initialization?!
        HEALERM = 'healer-m'
        HEALERF = 'healer-f'
        MAGEM = 'mage-m'
        MAGEF = 'mage-f'
        HEALERS = (HEALERM, HEALERF)
        MAGES = (MAGEM, MAGEF)
        PLAYERNAMES = (HEALERS, MAGES)
        name = random.choice(PLAYERNAMES[number])
        super(Player, self).__init__(name, rect)
        self.type = self.PLAYER
        self.status = self.DEAD
        self.DEATHTIME = 36
        self.RESPAWNTIME = 3
        self.DYINGTIME = 3
        self.deathTimer = self.DEATHTIME
        self.respawnTimer = self.RESPAWNTIME
        self.direction = self.DOWN
        self.speed = 3
        if self.name in HEALERS:
            self.missile.name = HOLYMISSILE
        elif self.name in MAGES:
            self.missile.name = MAGICMISSILE

    def setOutpositions(self, size):
        # Coordinates for the off-battlefield position of each player
        OUTPOSITIONS1 = []
        OUTPOSITIONS2 = []
        for i in range(64, 144, 16):
            OUTPOSITIONS1.append((i, 24, size, size))
        for i in range(328, 408, 16):
            OUTPOSITIONS2.append((i, 24, size, size))
        self.OUTPOSITIONS = (OUTPOSITIONS1,
                             OUTPOSITIONS2)

    def doRespawn(self):
        self.name = self.respawnList[self.respawns-1].name
        del self.respawnList[self.respawns-1]
        self.respawns -= 1
        #if self.status != self.DEAD:
        #    self.startCycle(self.ARRIVING)
        #else:
            # Player becomes STANDING when fully dead?
        #    self.startCycle(self.STANDING)
        self.startCycle(self.ARRIVING)
        self.y += 2

    def respawn(self, point):
        self.x = point[0]
        self.y = point[1]
        self.status = NOTHING
        if self.respawns > 0:
            self.respawns -= 1
            return True
        else:
            self.dead = True
            return False

    def startCycle(self, status):
        self.status = status
        centerX, centerY = self.center
        self.center = (centerX, centerY)
        self.step = self.STARTCYCLE

    def advanceCycles(self):
        if self.status == self.ARRIVING:
            if self.step < self.PLAYERCYCLES[self.ARRIVING]:
                self.step += 1
            else:
                self.status = self.INVULNERABLE
                self.step = self.STARTCYCLE
                self.direction = self.DOWN
                self.respawnStartTime = time.time()
        elif self.status == self.INVULNERABLE:
            if time.time() - self.respawnStartTime > INVULNERABLETIME:
                self.status = self.STANDING
                self.direction = self.DOWN
            else:
                if self.step < self.PLAYERCYCLES[self.INVULNERABLE]:
                    self.step += 1
                else:
                    self.step = self.RESTARTCYCLE
        elif self.status == self.MOVING:
            if self.step < self.PLAYERCYCLES[self.MOVING]:
                self.step += 1
            else:
                self.step = self.RESTARTCYCLE
        elif self.status == self.CASTING:
            if self.step < self.PLAYERCYCLES[self.CASTING]:
                self.step += 1
            else:
                self.status = self.STANDING
                self.step = self.STARTCYCLE
            if self.step == 3:
                self.fireMissile()
        elif self.status == self.DYING:
            if self.step < self.PLAYERCYCLES[self.DYING]:
                self.step += 1
            else:
                self.status = self.DEAD
                self.step = self.STARTCYCLE


class RespawnSprite(Player):
    def __init__(self, playernum, respawnnum, size):
        self.setOutpositions(size)
#        self.name = random.choice(self.PLAYERNAMES[playernum])
        startRect = self.OUTPOSITIONS[playernum][respawnnum]
        super(RespawnSprite, self).__init__(playernum, startRect)


class NewPlayer(Player):
    def __init__(self, playernum, x, y, width, height):
        startRect = (x, y, width, height)
        # Create all of the respawns for this player
        self.respawns = 3
        self.respawnList = []
        for i in range(self.respawns):
            self.respawnList.append(RespawnSprite(playernum, i, width))
        name = self.respawnList[0].name
        super(NewPlayer, self).__init__(playernum, startRect)
        self.setOutpositions(width)
#        self.setSizes(size)
        self.type = PLAYER
        self.playerNum = playernum
        self.outPosition = self.OUTPOSITIONS[playernum][i]
        startRect = self.outPosition


class Monster(LabyrinthObject):
    def __init__(self, name, x, y, width, height):
        super(Monster, self).__init__(name, (x, y, width, height))
        self.type = self.MONSTER
        self.step = 0
        self.aiMoveDecided = False
        self.walls = 0
        self.aiMoveDirection = self.RIGHT
        self.status = self.ARRIVING
        self.shotdelaytop = 25
        self.shotDelay = self.shotdelaytop
        self.dirChangeTime = time.time()
        self.spawnDelay = 0
        self.direction = self.DOWN
        self.aiPercentage = 50
        self.DIRCHANGEFREQ = 16
        self.FIREFREQUENCY = 4
        self.missile.fireTime = time.time()
        self.missile.name = DARKMISSILE
        self.BASESPEED = 2
        self.value = 100

    def findPaths(self, wallmaze):
        cellWalls = wallmaze.getCellWalls(self.centerx, self.centery)
        possiblePaths = [self.UP, self.LEFT, self.DOWN, self.RIGHT]
#        print cellWalls.wallsdict
        if cellWalls.getWall(NORTH):
            possiblePaths.remove(self.UP)
        if cellWalls.getWall(WEST):
            possiblePaths.remove(self.LEFT)
        if cellWalls.getWall(SOUTH):
            possiblePaths.remove(self.DOWN)
        if cellWalls.getWall(EAST):
            possiblePaths.remove(self.RIGHT)
        return possiblePaths

    def aiUndecide(self):
        self.aiMoveDecided = False

    def aiChooseDirection(self, maze, targets):
        # Decide which direction to go
        monsterDirection = self.direction
        if not self.aiMoveDecided:
            # Remove the direction opposite current direction unless its time
            chooseDirection = maze.getAllPaths(self.centerx, self.centery)
            if chooseDirection.count(self.DIRECT2OPPOSITE[monsterDirection]):
                if time.time() - self.dirChangeTime < self.DIRCHANGEFREQ:
                    chooseDirection.remove(self.DIRECT2OPPOSITE[monsterDirection])
                else:
                    self.dirChangeTime = time.time()
#            # Remove some directions that would take monster away from players
#            if random.randint(0, 99) < self.aiPercentage:
#                thisBox = maze.getCell(self.x, self.y)
#                targetDist = 100
#                targetDirectX = self.RIGHT
#                targetDirectY = self.DOWN
#                for target in targets:
#                    targetBox = maze.getCell(target.x, target.y)
#                    newTargetDist = thisBox.getDist(targetBox)
#                    if (target.status not in OBJECTINERT
#                        and newTargetDist < targetDist):
#                            targetDist = newTargetDist
#                            #print("Target Distance: " + str(targetDist))
#                            for direct in (self.UP, self.LEFT,
#                                           self.DOWN, self.RIGHT):
#                                moveBox = maze.getNextBox(self.x, self.y,
#                                                          direct)
#                                moveDist = moveBox.getDist(targetBox)
#                                if moveDist > targetDist:
#                                    if (direct in chooseDirection
#                                        and len(chooseDirection) > 1):
#                                            chooseDirection.remove(direct)
#                                            #print("Removed: " + VERBAL[direct] + " - " + str(moveDist - targetDist) + " farther")
#            #print("Target Distance: " + str(targetDist))
            # Pick a random direction based on remaining choices
            if len(chooseDirection) > 0:
                intentDirection = random.choice(chooseDirection)
            else:
                intentDirection = self.DOWN
#            self.aiMoveDirection = intentDirection
#            self.aiMoveDecided = True
        else:
            intentDirection = self.aiMoveDirection
        return intentDirection

    def moveOnPath(self, maze, intentdirection, speed):
        cell = maze.getCell(self.centerx, self.centery)
        if not cell.collidepoint(self.centerx, self.centery):
            self.aiUndecide()
        moveDirect, moveDist = super(Monster, self).moveOnPath(maze, intentdirection, speed)
        cellWalls = maze.getCell(self.centerx, self.centery)
        # Change direction if we're up against a wall
        GODIRECTIONS = {
            self.LEFT: gameobjects.WEST,
            self.DOWN: gameobjects.SOUTH,
            self.UP: gameobjects.NORTH,
            self.RIGHT: gameobjects.EAST
        }
        if (moveDirect == intentdirection and
            cellWalls.getWall(GODIRECTIONS[intentdirection], maze.WALLSDICT)
            and moveDist == 0):
                self.aiMoveDecided = False
                #print("collide wall" + str(time.time()))
                self.dirChangeTime = time.time()
        return moveDirect, moveDist

    def aiDecideFire(self, wallmaze, targets):
        # Return True if monster is in missile range of a player
        if time.time() - self.missile.fireTime > self.FIREFREQUENCY:
            for target in targets:
                if (self.targetInRange(wallmaze, target)
                        and target.status not in self.OBJECTINERT):
                    return True
        return False

    def fireMissile(self):
        super(Monster, self).fireMissile()
#        print("Fire Missile")
        self.missile.fireTime = time.time()

    def startCycle(self, status):
        self.status = status
        centerX, centerY = self.center
        self.center = (centerX, centerY)
        self.step = self.STARTCYCLE

    def advanceCycles(self):
        if self.status == self.ARRIVING:
            if self.step < self.MONSTERCYCLES[self.ARRIVING]:
                self.step += 1
            else:
                self.startCycle(self.MOVING)
        elif self.status == self.MOVING:
            if self.step < self.MONSTERCYCLES[self.MOVING]:
                self.step += 1
            else:
                self.step = self.RESTARTCYCLE
        elif self.status == self.DYING:
            if self.missile.fired:
                self.missile.status = self.DEPARTING
            if self.step < self.MONSTERCYCLES[self.DYING]:
                self.step += 1
            else:
                self.status = self.DEAD
#                self.startCycle(ORBITING)
#                self.orbStartTime = time.time()
        elif self.status == self.ORBITING:
            if self.step < self.MONSTERCYCLES[self.ORBITING]:
                self.step += 1
            # If its spirit is not collected in time, it's resorbed by the
            # Labyrinth
            elif time.time() - self.orbStartTime > ORBTIME:
                self.status = self.RESPAWNING
                self.step = self.STARTCYCLE
            else:
                self.step = self.RESTARTCYCLE
        elif self.status == DEPARTING:
            if self.step < MONSTERCYCLES[DEPARTING]:
                self.step += 1
            else:
                self.status = DEAD

    def centerSelf(self):
        self.center = self.topleft


# Lesser Monster Lv1
class Ghoul(Monster):
    def __init__(self, x, y, width, height):
        super(Ghoul, self).__init__('ghoul', x, y, width, height)
        self.points = 100
        self.speed = SPEED1
        self.aiPercentage = 40


# Lesser Monster Lv2
#class Ghast(Monster):
#    def __init__(self, x, y, width, height):
#        self.name = GHAST
#        self.points = 200
#        self.speed = SPEED2
#        self.aiPercentage = 50
#        super(Ghast, self).__init__(self.name, x, y, width, height)


# Lesser Monster Lv3
#class Wight(Monster):
#    def __init__(self, x, y, width, height):
#        self.name = WIGHT
#        self.points = 250
#        self.speed = SPEED3
#        self.aiPercentage = 60
#        super(Wight, self).__init__(self.name, x, y, width, height)


# Middle Boss Monster
class Ghost(Monster):
    def __init__(self, x, y, width, height):
        self.name = GHOST
        self.points = 500
        self.speed = SPEED4
        super(Ghost, self).__init__(self.name, x, y, width, height)
        self.width = 32
        self.height = 64


# Big Boss Monster
class Shade(Monster):
    def __init__(self, x, y, width, height):
        self.name = SHADE
        self.points = 1000
        self.speed = SPEED4
        super(Shade, self).__init__(self.name, x, y, width, height)
        self.width = 64
        self.height = 64


class LabyrinthLevel(gameobjects.WallMaze):
    def __init__(self, tileSize, spriteSize, displayWidth, displayHeight, wallmap, xparams={}):
        if 'yoffset' in xparams:
            yOffSet = xparams['yoffset']
        else:
            yOffSet = 0
        # Figure out the tile size
        cols = len(wallmap[0])
        rows = len(wallmap)
        width = tileSize * cols
        height = tileSize * rows
        # Center the playing field horizonally and slightly lower vertically
        x = (displayWidth - width) / 2
        y = ((displayHeight - height) / 2) + yOffSet
        # Set the persistent sizes
        self.TILESIZE = tileSize
        self.SPRITESIZE = spriteSize
        # Initialize the parent object
        super(LabyrinthLevel, self).__init__((x, y, width, height), wallmap)
        # Tiles that have walls ordered by direction
        self.WALLSDICT = {
            NORTH: (NN, NNN, NE, NNE, ENE, NNEE,
                    NS, NNS, SNS, NW, NNW, WNW,
                    NNWW, DNW, DNE, 1),
            EAST:  (EE, EEE, NE, NNE, ENE, NNEE,
                    WE, WWE, EWE, SE, SSE, ESE,
                    SSEE, DNE, 19, 48, 1),
            SOUTH: (SS, SSS, SE, SSE, ESE, SSEE,
                    NS, NNS, SNS, SW, SSW, WSW,
                    SSWW, 47, 48, 19),
            WEST:  (WW, WWW, NW, NNW, WNW, NNWW,
                    WE, WWE, EWE, SW, SSW, WSW,
                    SSWW, DNW, 19, 47, 1)
        }

    def getInCenterOfBox(self, x, y):
        (left, top) = self.getLeftTopOfCell(x, y)
        centerX = left + (self.CELLWIDTH / 2)
        centerY = top + (self.CELLHEIGHT / 2)
        # TODO: magic number!
#        centerY += 4  # Compensate for offset from center
        centerY -= 4  # Compensate for offset from center
        return (centerX, centerY)

    def getPlayerSpawnPoint(self, number=None):
        if number == None:
            x, y = random.choice(self.STARTPOSITIONS)
        else:
            x, y = self.STARTPOSITIONS[number]
        return self.getInCenterOfBox(x, y)

    def getMonsterSpawnPoint(self):
        y = random.randint(0, len(self.wallmap)-1)
        x = random.randint(0, len(self.wallmap[0])-1)
        return self.getInCenterOfBox(x, y)

    # Extend gameobjects.WallMaze.getPathIsOpen() so we can supply the WALLSDICT
    def getPathIsOpen(self, pathDirection, objRect):
        cellRect = self.getCell(objRect.centerx, objRect.centery).getRect()
        cellWalls = self.getCell(objRect.centerx, objRect.centery).getWalls()
        pathIsOpen = True
        if pathDirection == self.UP and cellWalls in self.WALLSDICT[NORTH]:
            if objRect.top <= cellRect.top:
                pathIsOpen = False
        if pathDirection == self.LEFT and cellWalls in self.WALLSDICT[WEST]:
            if objRect.left <= cellRect.left:
                pathIsOpen = False
        if pathDirection == self.DOWN and cellWalls in self.WALLSDICT[SOUTH]:
            if objRect.bottom >= cellRect.bottom:
                pathIsOpen = False
        if pathDirection == self.RIGHT and cellWalls in self.WALLSDICT[EAST]:
            if objRect.right >= cellRect.right:
                pathIsOpen = False
        return pathIsOpen

    def getAllPaths(self, x, y):
        cellWalls = self.getCell(x, y)
        allPaths = [self.UP, self.LEFT, self.DOWN, self.RIGHT]
#        print(cellWalls)
#        cellWalls = wallmaze.getCellWalls(self.centerx, self.centery)
#        walls = cellWalls.getWalls()
#        if walls in wallmaze.WALLSDICT[COMPASS[newDirect]]:
        walls = self.getCell(x, y).getWalls()
        if walls in self.WALLSDICT[NORTH]:
            allPaths.remove(self.UP)
        if walls in self.WALLSDICT[WEST]:
            allPaths.remove(self.LEFT)
        if walls in self.WALLSDICT[SOUTH]:
            allPaths.remove(self.DOWN)
        if walls in self.WALLSDICT[EAST]:
            allPaths.remove(self.RIGHT)
        return allPaths

    def getVerbalPaths(self, paths):
        directs = []
        for direct in paths:
            directs.append(VERBAL[direct])
        return directs


class Maze1(LabyrinthLevel):
    def __init__(self, tileSize, spriteSize, displayWidth, displayHeight, xparams={}):
        # Map the walls
        wallmap = [
            [ 00,    NNWW, NNE,  NNW,  NNS,  NNS,  NNS,  NNS,  NNN,  NNEE ],
            [ NNWW,  48,   WW,   EE,   NW,   NN,   NE,   NW,   SE,   EWE  ],
            [ WSW,   NN,   EE,   WW,   OO,   EE,   WW,   SS,   NE,   EWE  ],
            [ WNW,   EE,   WE,   WE,   WE,   WE,   WE,   NW,   40,   SSEE ],
            [ WWW,   SS,   OO,   OO,   EE,   WW,   OO,   EE,   47,   NNEE ],
            [ SSWW,  SNS,  SSS,  SSE,  SSW,  SSE,  SSW,  SSS,  SNS,  SSEE ]
        ]
        # Set some player spawning positions
        self.STARTPOSITIONS = ((9, 1),
                               (0, 1),
                               (9, 5),
                               (0, 5))
        # Initialize the parent object
        super(Maze1, self).__init__(tileSize, spriteSize, displayWidth, displayHeight, wallmap, xparams)


class Maze2(LabyrinthLevel):
    def __init__(self, tileSize, spriteSize, displayWidth, displayHeight, xparams={}):
        # Map the walls
        wallmap = [
            [ NNWW, NNN,  NNN,  NNN,  NNE,  NNW,  NNS,  NNS,  NNN,  NNEE ],
            [ WWW,  OO,   OO,   EE,   WW,   OO,   NE,   NW,   SE,   EWE  ],
            [ WSW,  OO,   OO,   OO,   OO,   OO,   OO,   SS,   NN,   ESE  ],
            [ WNW,  OO,   OO,   OO,   OO,   OO,   OO,   NN,   OO,   ENE  ],
            [ WWW,  OO,   OO,   OO,   OO,   OO,   OO,   OO,   OO,   EEE  ],
            [ SSWW, SSS,  SSS,  SSS,  SSE,  SSW,  SSS,  SSS,  SSS,  SSEE ]
        ]
        # Set some player spawning positions
        self.STARTPOSITIONS = ((8, 2),
                               (0, 2),
                               (8, 4),
                               (0, 4))
        # Initialize the parent object
        super(Maze2, self).__init__(tileSize, spriteSize, displayWidth, displayHeight, wallmap, xparams)


class Maze3(LabyrinthLevel):
    def __init__(self, tileSize, spriteSize, displayWidth, displayHeight, xparams={}):
        # Map the walls
        wallmap = [
            [ NNWW, NNN,  NNN,  NNE,  NNW,  NNS,  NNN,  NNEE ],
            [ WSW,  OO,   OO,   OO,   OO,   SS,   NN,   SSEE ],
            [ WNW,  OO,   OO,   OO,   OO,   NN,   OO,   NNEE ],
            [ SSWW, SSS,  SSS,  SSE,  SSW,  SSS,  SSS,  SSEE ]
        ]
        # Set some player spawning positions
        self.STARTPOSITIONS = ((8, 2),
                               (0, 2),
                               (8, 5),
                               (0, 5))
        # Initialize the parent object
        super(Maze3, self).__init__(tileSize, spriteSize, displayWidth, displayHeight, wallmap, xparams)


class Maze4(LabyrinthLevel):
    def __init__(self, tileSize, spriteSize, displayWidth, displayHeight, xparams={}):
        # Map the walls
        wallmap = [
            [ NNWW,  NNN,  NNN,  NNN,  NNE,  NNW,  NNN,  NNN,  NNN,  NNEE ],
            [  WWW,   OO,   SE,   WW,   OO,   OO,   EE,   SW,   OO,   EEE ],
            [  WSW,   EE,   NW,   EE,   WW,   EE,   WW,   NE,   WW,   ESE ],
            [  WNW,   SS,   EE,   SW,   EE,   WW,   SE,   WW,   SS,   ENE ],
            [  WWE,   NW,   EE,   NW,   OO,   OO,   NE,   WW,   NE,   EWE ],
            [ SSWW,  SSS,  SSS,  SSS,  SSE,  SSW,  SSS,  SSS,  SSS,  SSEE ]
        ]
        # Set some player spawning positions
        self.STARTPOSITIONS = ((8, 2),
                               (0, 2),
                               (8, 4),
                               (0, 4))
        # Initialize the parent object
        super(Maze4, self).__init__(tileSize, spriteSize, displayWidth, displayHeight, wallmap, xparams)


class MainMaze1(LabyrinthLevel):
    def __init__(self, tileSize, spriteSize, displayWidth, displayHeight, xparams={}):
        # Map the walls
        wallmap = [
#            [ NNWW,  NNS, NNEE, NNWW,  NNS,  NNS, NNEE, NNWW,  NNS, NNEE ],
            [ NNWW,  NNS, NNEE,    0,    8,    8,    0, NNWW,  NNS, NNEE ],
            [ WWW,    NN,   16,  NNN,   24,   24,  NNN,   27,   NN,  EEE ],
            [ WWW,    41,  SSS,   10,   41,   10,   41,  SSS,   10,  EEE ],
            [ SSWW, SSEE,   00, SSWW,   16,   27, SSEE,   00, SSWW, SSEE ],
            [ 00,     00,   00,   00,   35,   35,   00,   00,   00,   00 ],
            [ 00,     00,   00,   00,   00,   00,   00,   00,   00,   00 ],
        ]
        # Set some player spawning positions
        self.STARTPOSITIONS = ((8, 1),
                               (1, 1))
        # Initialize the parent object
        super(MainMaze1, self).__init__(tileSize, spriteSize, displayWidth, displayHeight, wallmap, xparams)


class HubMaze1(LabyrinthLevel):
    def __init__(self, tileSize, spriteSize, displayWidth, displayHeight, xparams={}):
        # Map the walls
        wallmap = [
#            [ NNWW,  NNS, NNEE, NNWW,  NNS,  NNS, NNEE, NNWW,  NNS, NNEE ],
            [ 00,     00,   00,   00,   00,   00,   00,   00,   00,   00 ],
            [ 00,   NNWW,  NNN, NNEE,    1,    1, NNWW,  NNN, NNEE,   00 ],
            [ 00,    WWW,   OO,   16,    8,    8,   27,   OO,  EEE,   00 ],
            [ 00,   SSWW,  SSS,   10,   OO,   OO,   41,  SSS, SSEE,   00 ],
            [ 00,     00,   00, SSWW,   35,   35, SSEE,   00,   00,   00 ],
            [ 00,     00,   00,   00,   19,   19,   00,   00,   00,   00 ],
        ]
        # Set some player spawning positions
        self.STARTPOSITIONS = ((7, 2),
                               (2, 2))
        # Initialize the parent object
        super(HubMaze1, self).__init__(tileSize, spriteSize, displayWidth, displayHeight, wallmap, xparams)


class Maze1large(LabyrinthLevel):
    def __init__(self, tileSize, spriteSize, displayWidth, displayHeight, xparams={}):
        # Map the walls
        wallmap = [[ NNWW,  NNS,  NNN,  NNE,  NNW,  NNN, NNE,  NNW,  NNN,  NNS,  NNEE ],
                   [ WWE,   0,    SE,   WW,   EE,   WE,  WW,   EE,   SW,   0,   EWE ],
                   [ WSW,   EE,   NW,   EE,   SW,   OO,  SE,   WW,   NE,   WW,   ESE ],
                   [ WNW,   SS,   EE,   SW,   NN,   SS,  NN,   SE,   WW,   SS,   ENE ],
                   [ WWW,   NS,   OO,   NE,   WW,   NS,  EE,   NW,   OO,   NS,   EEE ],
                   [ WWE,   NW,   EE,   WW,   SS,   NN,  SS,   EE,   WW,   NE,   EWE ],
                   [ SSWW,  SSE,  SSW,  SSS,  SNS,  SSS, SNS,  SSS,  SSE,  SSW,  SSEE ]]
        # Initialize the parent object
        super(Maze1, self).__init__(tileSize, spriteSize, displayWidth, displayHeight, wallmap, xparams)

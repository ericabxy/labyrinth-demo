import pygame, unittest, gameobjects

class TestGameObjects(unittest.TestCase):
    def setUp(self):
        self.screenW = 50
        self.screenH = 50
        self.cellRect = pygame.Rect(50, 50, 12, 12)
        self.sprite = gameobjects.WallMazeSprite("Test Sprite", (0, 0, 8, 8))
        self.sprite.center = self.cellRect.topleft
        self.startX = self.sprite.x
        self.startY = self.sprite.y

    def test_moveInCenter(self):
        for dist in range(0, 16):
            for x in range(0, self.screenW):
                for y in range(0, self.screenH):
                    for direct in (gameobjects.ANYORTHOGONAL):
                        self.sprite.topleft = (x, y)
                        newRect, newDirect = self.sprite.getInCenter(self.cellRect, direct, dist)
                        self.sprite.clamp(newRect)
                        if newDirect == direct:
                            if dir in (gameobjects.LEFT, gameobjects.RIGHT):
                                self.assertEqual(self.sprite.centery, self.cellRect.centery)
                            elif dir in (gameobjects.UP, gameobjects.DOWN):
                                self.assertEqual(self.sprite.centerx, self.cellRect.centerx)

if __name__ == '__main__':
    unittest.main()

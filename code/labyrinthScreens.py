# Copyright 2015, 2016, 2017 Eric Duhamel

# This file is part of Labyrinth.

# Labyrinth is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Labyrinth is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Labyrinth.  If not, see <http://www.gnu.org/licenses/>.

import os, time

from ebigo import graphics

HUBMODE = 'hubmode'
COOPMODE = 'coopmode'

SCREENHEIGHT = 240

BGCOLOR = (33, 30, 39)

# These functions are to aid in the creation of "screen parts" to be passed to
# Graphics.drawScreen() for rendering
class GameScreen(object):
    """Construct a screen made up of text, images, and tilemaps."""
    def __init__(self, screenman):
        self.screenManager = screenman
        self.timers = []
        self.timerNum = self.setTimer()
        self.redraw = True
        self.screenParts = []

    def getText(self, textlist, x, y):
        return {'list': textlist, 'x': x, 'y': y, 'xparams': {}}

    def getImage(self, filename, x, y):
        return {'filename': filename, 'x': x, 'y': y, 'xparams': {}}

    def getPlacement(self, screenpart, alignment='topleft', foreground=False):
        screenPart = screenpart
        if 'xparams' not in screenPart:
            xparams = {}
        else:
            xparams = screenPart['xparams']
        xparams['alignment'] = alignment
        xparams['foreground'] = foreground
        screenPart['xparams'] = xparams
        del xparams
        return screenPart

    def getScale(self, screenpart, scale):
        screenPart = screenpart
        if 'xparams' not in screenPart:
            xparams = {}
        else:
            xparams = screenPart['xparams']
        if scale > 1:
            xparams['scale'] = scale

    def getFontWorks(self, screenpart, fontcolor, fontsize=16, fontname=None):
        screenPart = screenpart
        if 'xparams' not in screenPart:
            xparams = {}
        else:
            xparams = screenPart['xparams']
        xparams['fontcolor'] = fontcolor
        xparams['fontsize'] = fontsize
        xparams['fontname'] = fontname
        screenPart['xparams'] = xparams
        del xparams
        return screenPart

    def getScreenParts(self, maps=(), images=(), texts=()):
        return {'maps': maps, 'images': images, 'texts': texts}

    def setTimer(self):
        self.timers.append(time.time())
        return len(self.timers)-1

    def getTimer(self, index):
        if len(self.timers) > index:
            return time.time() - self.timers[index]
        else:
            return None

    def resetTimer(self, index):
        if len(self.timers) > index:
            self.timers[index] = time.time()
            return True
        else:
            return False

    def setScreenManager(self, screenman):
        self.screenManager = screenman

    def addText(self, textlist, alignment, xargs={}):
        textImage = self.screenManager.addText(textlist, alignment, xargs)
        self.screenParts.append(
            textImage
        )
        return textImage

    def updateTexts(self, variables={}):
        return None

    def hide(self):
        for screenPart in self.screenParts:
            screenPart.kill()
        return None


LOADINGSCREEN = "Loading Screen"
class LoadingScreen(GameScreen):
    def __init__(self, gfx):
        super(LoadingScreen, self).__init__(gfx)
        self.name = LOADINGSCREEN
        self.addText(
            ("Loading...",),
            gfx.CENTER,
        )
        self.TIME = 1
        self.gameMode = HUBMODE


LICENSESCREEN = "License Screen"
class LicenseScreen(GameScreen):
    def __init__(self, gfx):
        super(LicenseScreen, self).__init__(gfx)
        self.name = LICENSESCREEN
        filename = os.path.join('data', 'GPL.png')
        gfx.hide(BGCOLOR)
        gfx.addImage(filename, gfx.CENTER)
        self.TIME = 2
        self.gameMode = HUBMODE


POWEREDBYSCREEN = "Poweredby Screen"
class PoweredByScreen(GameScreen):
    def __init__(self, gfx):
        super(PoweredByScreen, self).__init__(gfx)
        self.name = POWEREDBYSCREEN
        filename = os.path.join('data', 'Pygame_lowres.png')
        gfx.hide(BGCOLOR)
        gfx.addImage(filename, gfx.CENTER)
        self.TIME = 2
        self.gameMode = HUBMODE


TITLESCREEN = "Title Screen"
class TitleScreen(GameScreen):
    def __init__(self, codedir, gfx):
        super(TitleScreen, self).__init__(gfx)
        self.name = TITLESCREEN
        textfile = open(os.path.join(codedir, 'VERSION'))
        version = textfile.read()
        gfx.hide(BGCOLOR)
        gfx.addText(
            ("LABYRINTH", "of the DEAD"),
            gfx.MIDTOP,
            {
             'fontname': os.path.join('data', 'alagard.ttf'),
             'fontsize': 72,
             'fontcolor': gfx.PURPLE
            }
        )
        self.addText(
            ("Labyrinth of the Dead  Copyright (C)  2015, 2016, 2017 Eric Duhamel",
             "This program comes with ABSOLUTELY NO WARRANTY.",
             "This is free software, and you are welcome to redistribute it",
             "under certain conditions."),
            gfx.BOTTOMLEFT,
            {
             'fontname': os.path.join('data', 'romulus.ttf'),
             'fontsize': 16,
             'fontcolor': gfx.GRAY
            }
        )
        self.addText(
            ("ALPHA",),
            gfx.TOPRIGHT,
            {
             'fontname': os.path.join('data', 'romulus.ttf'),
             'fontsize': 20,
             'fontcolor': gfx.WHITE
            }
        )
        self.addText(
            (version[:-1],),
            gfx.BOTTOMRIGHT,
            {
             'fontname': os.path.join('data', 'romulus.ttf'),
             'fontsize': 16,
             'fontcolor': gfx.GRAY
            }
        )
        self.addText(
            ("Press to Start",),
            gfx.CENTER,
            {
             'fontname': os.path.join('data', 'romulus.ttf'),
             'fontsize': 18,
             'fontcolor': gfx.WHITE
            }
        )
        self.TIME = 90
        self.gameMode = HUBMODE


HUBSCREEN = "Hub Screen"
class HubScreen(GameScreen):
    def __init__(self, gfx, gamestate):
        super(HubScreen, self).__init__(gfx)
        self.name = HUBSCREEN
        gfx.hide(BGCOLOR)
        maprect = gamestate.getFieldRect()
        tilemap = gamestate.getTileMap()
        gfx.drawTileMap(
            'walls',
            maprect,
            tilemap,
            graphics.BACKGROUND
        )
        gfx.addImage(
            os.path.join('data', 'Controls.png'),
            gfx.TOPLEFT
        )
        gfx.addText(
            ("LABYRINTH", "of the DEAD"),
            gfx.MIDTOP,
            {
                'fontname': os.path.join('data', 'alagard.ttf'),
                'fontsize': 72,
                'fontcolor': gfx.PURPLE
            }
        )
        self.TEXT2 = gfx.addText(
            ("Ready: ",),
            gfx.MIDBOTTOM,
            {'fontname': os.path.join('data', 'romulus.ttf')}
        )
        self.TIME = 90
        self.gameMode = HUBMODE

    def updateTexts(self, ready):
        self.TEXT2.changeText(("Ready: " + str(ready),))


GAMEPLAYSCREEN = "Game Play Screen"
class GamePlayScreen(GameScreen):
    def __init__(self, gfx, gamestate):
        super(GamePlayScreen, self).__init__(gfx)
        self.name = GAMEPLAYSCREEN
        maprect = gamestate.getFieldRect()
        tilemap = gamestate.getTileMap()
        gfx.hide(BGCOLOR)
        gfx.drawTileMap(
            'walls',
            maprect,
            tilemap,
            graphics.BACKGROUND
        )
        self.notice1 = self.addText(
            ("Level Notice",),
            gfx.MIDTOP,
            {'fontsize': 24,
             'fontname': os.path.join('data', 'romulus.ttf')}
        )
        self.TIME = 2
        self.gameMode = COOPMODE
        self.updated = 0  # TODO: deprecated

    def updateTexts(self, notices, gfx):
        self.updated += 1  # TODO: deprecated
        monsterqueue = notices['monsterqueue']
        monsterspeed = notices['monsterspeed']
        status = notices['status']
        level = notices['level']
        maxlevel = notices['maxlevel']
        if status == 'starting':
            self.notice1.changeText(
                ("Here They Come",)
            )
        elif status == 'playing':
            self.notice1.changeText(
                ("Wave " + str(level) + " of " + str(maxlevel),)
            )
        elif status == 'ending':
            self.notice1.changeText(
                ("Dungeon Cleared",)
            )
        elif status == 'gameover':
            self.notice1.changeText(
                ("Game Over",)
            )
        GAMESCREENTEXT1 = self.getText(
            ("monsterQueue: " + str(len(monsterqueue)),),
            gfx.HALFWIDTH,
            gfx.TOPOFSCREEN+15
        )
        self.getPlacement(
            GAMESCREENTEXT1, graphics.CENTERED, graphics.FOREGROUND
        )
        self.getFontWorks(
            GAMESCREENTEXT1, graphics.NORMALFONTCOLOR, graphics.NORMALFONTSIZE,
            os.path.join('data', 'romulus.ttf')
        )
        GAMESCREENTEXT2 = self.getText(
            ("monsterSpeed: " + str(monsterspeed),),
            gfx.HALFWIDTH,
            gfx.TOPOFSCREEN+27
        )
        self.getPlacement(
            GAMESCREENTEXT2, graphics.CENTERED, graphics.FOREGROUND
        )
        self.getFontWorks(
            GAMESCREENTEXT2, graphics.NORMALFONTCOLOR, graphics.NORMALFONTSIZE,
            os.path.join('data', 'romulus.ttf')
        )
        GAMESCREENTEXT1['xparams']['blank'] = True
        GAMESCREENTEXT2['xparams']['blank'] = True
        GAMESCREENTEXTS = ()
        return {'texts': GAMESCREENTEXTS,}

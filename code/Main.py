# Copyright 2015, 2016, 2017 Eric Duhamel

# This file is part of Labyrinth.

# Labyrinth is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Labyrinth is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Labyrinth.  If not, see <http://www.gnu.org/licenses/>.

# TODO: pygame should be called by submodules, not Main
import pygame

# Import engine and game modules
from ebigo import graphics
from ebigo import sounds
from ebigo import controls
import labyrinthGame
import labyrinthScreens
import labyrinthMeta

FULLSCREENPARAM = '--fullscreen'
DEBUGPARAM = '--debug'
NOSOUNDPARAM = '--nosound'
NOMUSICPARAM = '--nomusic'

# Clock for regulating game speed
FPSCLOCK = pygame.time.Clock()

def main(args, datadir, codedir):
    # Default parameters
    displayMode = graphics.WINDOWED
    debugMode = False
    soundOn = True
    musicOn = True
    # Process command-line arguments
    if DEBUGPARAM in args:
        debugMode = True
    if FULLSCREENPARAM in args:
        displayMode = graphics.FULLSCREEN
    if NOSOUNDPARAM in args:
        soundOn = False
        musicOn = False
    if NOMUSICPARAM in args:
        musicOn = False
    # Initialize graphics, sound, controls
    pygame.init()
    gfx = graphics.Graphics(
        labyrinthMeta.DISPLAYWIDTH,
        labyrinthMeta.DISPLAYHEIGHT,
        'Labyrinth',
        displayMode,
        debugMode
    )
    sound = sounds.Sounds()
    controlmap = controls.ControlMap()
    # Display the LOADING screen
    screenMode = labyrinthScreens.LoadingScreen(gfx)
    gfx.update()
    SCALE = 2
    labyrinthMeta.addAllAssets(sound, gfx, SCALE)

    # Initial screen mode
    WINDOWWIDTH = 640
    WINDOWHEIGHT = 480
    screenMode = labyrinthScreens.LicenseScreen(gfx)
    redrawScores = False
    redrawNotices = False

    # Initial game state
#    numPlayers = 2
#    gamestate = labyrinthGame.GamePlayObject(
#        WINDOWWIDTH, WINDOWHEIGHT, numPlayers, labyrinthGame.HUBMODE
#    )

    # MAIN PROGRAM LOOP
    done = False
    while not done:
        # GET INPUT
        # Update the state of all controllers and get any function
        # keypress. A function key lets the user perform optional
        # actions on the program.
        functionKey = controlmap.process_events()
        # Escape or Quit will terminate Labyrinth
        if functionKey == controls.ESCAPEKEY:
            print('Escape key pressed...')
            done = True
        # Placeholders for functions
        elif functionKey == controls.HELPKEY:
            Nothing = True
        elif functionKey == controls.OPTIONKEY:
            Nothing = None
        elif functionKey == controls.SELECTKEY:
            Nothing = None
        elif functionKey == controls.STARTKEY:
            sound.stopPlaylist()
#            gamestate = labyrinthGame.GamePlayObject(
#                WINDOWWIDTH,
#                WINDOWHEIGHT,
#                numPlayers,
#                labyrinthMeta.COOPMODE
#            )
            screenMode.hide()
            screenMode = labyrinthScreens.GamePlayScreen(
                    gfx,
                    gamestate
            )
        elif functionKey == controls.RESETKEY:
            print('Game reset...')
#            gamestate = labyrinthGame.GamePlayObject(
#                WINDOWWIDTH, WINDOWHEIGHT, numPlayers,
#                labyrinthGame.HUBMODE)
            screenMode.hide()
            screenMode = labyrinthScreens.LicenseScreen(gfx)
        elif functionKey == controls.SONGEND:
            sound.advancePlaylist()

        # These are the screen modes which act as splash screens, title screen,
        # They cycle through automatically, or by the user pressing any key.
        if screenMode.name == labyrinthScreens.LICENSESCREEN:
            if musicOn:
                sound.startPlaylist()
            if (controlmap.getAnyButtonAll() or
                screenMode.getTimer(screenMode.timerNum) > screenMode.TIME):
                    # Move on to License screen
                    screenMode.hide()
                    screenMode = labyrinthScreens.PoweredByScreen(gfx)
        elif screenMode.name == labyrinthScreens.POWEREDBYSCREEN:
            if (controlmap.getAnyButtonAll() or
                screenMode.getTimer(screenMode.timerNum) > screenMode.TIME):
                    # Set up the next screen
                    screenMode.hide()
                    screenMode = labyrinthScreens.TitleScreen(
                        codedir,
                        gfx
                    )
        # SHOW THE TITLE SCREEN
        elif screenMode.name == labyrinthScreens.TITLESCREEN:
            # Cycle through splash screens again after inactivity
            if screenMode.getTimer(screenMode.timerNum) > screenMode.TIME:
                screenMode.hide()
                screenMode = labyrinthScreens.LicenseScreen(gfx)
            elif controlmap.getAnyButtonAll():
                ONEPLAYERS = 1
                TWOPLAYERS = 2
                sound.stopPlaylist()
                gamestate = labyrinthGame.GamePlayObject(
                    WINDOWWIDTH, WINDOWHEIGHT, TWOPLAYERS,
                    labyrinthMeta.COOPMODE
                )
                for object in gamestate.getAllObjects():
                    gfx.addRect(object)
                screenMode.hide()
                screenMode = labyrinthScreens.GamePlayScreen(
                    gfx,
                    gamestate
                )
        # SHOW THE MAIN MENU
        elif screenMode.name == labyrinthScreens.HUBSCREEN:
            ONEPLAYERS = 1
            TWOPLAYERS = 2
            # First, get the current controls
            sendcontrollers = controlmap.getDicts()
            # Update the current game state
            returnInt = gamestate.playGame(sendcontrollers)
            if returnInt in (labyrinthGame.START1PCOOP,
                             labyrinthGame.START2PCOOP):
                sound.stopPlaylist()
                if returnInt == labyrinthGame.START1PCOOP:
                    gamestate = labyrinthGame.GamePlayObject(
                        WINDOWWIDTH, WINDOWHEIGHT, ONEPLAYERS,
                        labyrinthMeta.COOPMODE
                    )
                if returnInt == labyrinthGame.START2PCOOP:
                    gamestate = labyrinthGame.GamePlayObject(
                        WINDOWWIDTH, WINDOWHEIGHT, TWOPLAYERS,
                        labyrinthMeta.COOPMODE
                    )
                screenMode.hide()
                screenMode = labyrinthScreens.GamePlayScreen(
                    gfx,
                    gamestate
                )
            # Draw stuff and play sounds
            spriteObjs = gamestate.getAllObjs()
            gfx.drawAllSprites(spriteObjs)
            if soundOn:
                labyrinthMeta.playSoundEffects(
                    sound,
                    spriteObjs,
                    gamestate.mode,
                    gamestate.status,
                    gamestate.step
                )
            # Reset the timer if a button is pressed
            if controlmap.getAnyButtonAll():
                screenMode.resetTimer(screenMode.timerNum)
        # PLAY THE GAME
        elif screenMode.name == labyrinthScreens.GAMEPLAYSCREEN:
            # First, get the current controls
            sendcontrollers = controlmap.getDicts()
            # Play the game
            gameUpdate = gamestate.playGame(sendcontrollers)
            # Update the screen based on game events
            if gameUpdate in (
                    labyrinthGame.SCOREEVENT,
                    labyrinthGame.DEATHEVENT,
                    ):
                redrawScores = True
            elif gameUpdate in (
                    labyrinthGame.STARTEVENT,
                    gamestate.SPAWNEVENT,
                    labyrinthGame.ENDEVENT,
                    labyrinthGame.GAMEOVEREVENT
                    ):
                # TEMP: add new sprites on-the-fly when they are
                # added to the game
                for sprite in gamestate.spriteList:
                    gfx.addSprite(sprite)
                redrawNotices = True
            elif gameUpdate == labyrinthGame.ENDCOOPGAME:
                # Return to Copyright screen once game is over
                screenMode.hide()
                screenMode = labyrinthScreens.LicenseScreen(gfx)
                # TODO: labyrinthScreens should create a gamestate
                # object or None
                gamestate = labyrinthGame.GamePlayObject(
                    WINDOWWIDTH, WINDOWHEIGHT,
                    numPlayers, screenMode.gameMode
                )
            # Draw stuff and play sounds
            spriteObjs = gamestate.getAllObjs()
#            gfx.drawAllSprites(spriteObjs)
            gfx.updateSprites()
            if soundOn:
                labyrinthMeta.playSoundEffects(
                    sound,
                    spriteObjs,
                    gamestate.mode,
                    gamestate.status,
                    gamestate.step
                )
        if redrawScores or redrawNotices:
            if screenMode.name == labyrinthScreens.GAMEPLAYSCREEN:
                if redrawScores:
                    labyrinthMeta.showPlayerScores(
                        gamestate.getPlayerScores(),
                        gfx
                    )
                    redrawScores = False
                if redrawNotices:
                    gameStats = gamestate.getGameStatus()
                    updatedTexts = screenMode.updateTexts(gameStats, gfx)
                    if updatedTexts != None:
                        gfx.drawScreen(updatedTexts)
                    redrawScores = False
#            elif screenMode.name == labyrinthScreens.HUBSCREEN:
#                screenMode.updateTexts(gamestate.getPlayersReady())
        # After everything is drawn, update the screen
        gfx.update()
        # Limit frames per second
        FPSCLOCK.tick(labyrinthMeta.FPS)
    pygame.quit()

import sys
if __name__ == '__main__':
    main(sys.argv)

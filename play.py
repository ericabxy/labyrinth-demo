#!/usr/bin/env python
import sys, os

def main():
    #figure out our directories
    localpath = os.path.split(os.path.abspath(sys.argv[0]))[0]
    testdata = os.path.join(localpath, 'data')
    testcode = os.path.join(localpath, 'code')
    if os.path.isdir(testdata):
        DATADIR = os.path.join(testdata, '')
    if os.path.isdir(testcode):
        CODEDIR = testcode

    #apply our directories and test environment
    os.chdir(localpath)
    sys.path.insert(0, CODEDIR)

    #run game and protect from exceptions
    try:
        import Main as main
        main.main(sys.argv, DATADIR, CODEDIR)
        print('Main program ended...')
    except KeyboardInterrupt:
        print('Keyboard Interrupt (Control-C)...')

if __name__ == '__main__':
    main()
    sys.exit()

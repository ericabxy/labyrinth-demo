Please follow the style guidelines in PEP-0008 and PEP-0257. They are
included as text files in doc/. Also follow conventions based on
refactoring.com

Code contributions must be governed by the license described by the
file LICENSE -- the GNU General Public License version 3 or higher.
